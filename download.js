var WebTorrent = require('webtorrent');
var path = require('path');

var client = new WebTorrent();

var magnetURI = 'magnet:?xt=urn:btih:30f4530e7a27cb16ee229a70a3bd721d2787f86c&dn=JavaScript%20-%20Glossary%20On%20Demand%20%282016%29%20%28Pdf%29%20Gooner&&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80%2Fannounce&tr=udp%3A%2F%2Fglotorrents.pw%3A6969%2Fannounce&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80%2Fannounce&tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337%2Fannounce&tr=udp%3A%2F%2Fzer0day.to%3A1337%2Fannounce&tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969%2Fannounce';

client.add(magnetURI, { path: path.join(__dirname, 'destination') }, function (torrent) {
    console.log('Starting Download...');
    console.log('Path to download: ', torrent.path);
    torrent.on('download', function (bytes) {
        console.log('just downloaded: ' + bytes)
        console.log('total downloaded: ' + torrent.downloaded);
        console.log('download speed: ' + torrent.downloadSpeed)
        console.log('progress: ' + torrent.progress)
})
    torrent.on('done', function () {
        console.log('torrent download finished');
    });
});
